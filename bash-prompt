#!/bin/bash

export USE_GIT_PROMPT=0

git_prompt_toggle() {
  if [[ $USE_GIT_PROMPT == 1 ]]; then
    export USE_GIT_PROMPT=0
  else
    export USE_GIT_PROMPT=1
  fi
}

_build_prompt() {
  # function that combines information elements to create a bash prompt
  local color git_status tip_dir venv

  if [[ $USE_GIT_PROMPT != 0 ]]; then
    git_status=$(_show_git_status)
    if [[ -n "$git_status" ]]; then
      git_status=":${git_status}"
    fi
  fi

  # Just the tip
  tip_dir=$(basename "${PWD}")

  # change the shortname for our homedir to $HOME
  if [[ "$PWD" == "$HOME" && "$PWD" != '/root' ]]; then
    tip_dir='\$HOME'
  fi

  venv=
  if [[ -n "$VIRTUAL_ENV" ]]; then
    venv=' (venv)'
  fi

  # 30  black   31  red      32  green   33  yellow    34  blue      35  magenta
  # 36  cyan    37  lt gray  90  dk grey 91  dk red    92  lt green  93  lt ylw
  # 94  lt blue 95  lt mgnta 96  lt cyan 97  white

  # local prompt is green
  color=92

  # whatever host i'm on is localhost...
  host='localhost'

  if [[ -n $SSH_CLIENT ]]; then
    # remote prompt is yellow
    color=33

    # ... unless i'm sshing somewhere
    host='\h'
  fi

  if [[ $EUID -eq 0 ]]; then
    # root is red. friendly reminder to be a little more careful.
    color=91
  fi

  date=$(date +"%F %T")

  # colorize the hostname
  host="\[\e[${color}m\]${host}\[\e[0m\]"

  PS1="\[\a\]$host [$date] [${tip_dir}${git_status}]$venv\\\$ "
  return 0
}

_show_git_status() {
  # Get the current git branch and colorize to indicate branch state
  # branch_name+ indicates there are stash(es)
  # branch_name? indicates there are untracked files
  # branch_name! indicates your branches have diverged
  local unknown untracked stash clean ahead behind staged dirty diverged

  unknown='0;34'      # blue
  untracked='0;32'    # green
  stash='0;32'        # green
  clean='0;32'        # green
  ahead='0;33'        # yellow
  behind='0;33'       # yellow
  staged='0;96'       # cyan
  dirty='0;31'        # red
  diverged='0;31'     # red

  if [[ $TERM = *256color ]]; then
    unknown='38;5;20'     # dark blue
    untracked='38;5;76'   # mid lime-green
    stash='38;5;76'       # mid lime-green
    clean='38;5;82'       # brighter green
    ahead='38;5;226'      # bright yellow
    behind='38;5;142'     # darker yellow-orange
    staged='38;5;214'     # orangey yellow
    dirty='38;5;202'      # orange
    diverged='38;5;196'   # red
  fi

  branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)

  if [[ -n "$branch" ]]; then
    if [[ "$branch" == 'HEAD' ]]; then
      branch=$(git rev-parse --short HEAD 2>/dev/null)
    fi

    git_status=$(git status -u 2> /dev/null)

    # If nothing changes the color, we can spot unhandled cases.
    color=$unknown

    if [[ $git_status =~ 'Untracked files' ]]; then
      color=$untracked
      branch="${branch}?"
    fi

    if git stash show &>/dev/null; then
      color=$stash
      branch="${branch}+"
    fi

    if [[ $git_status =~ working.*clean ]]; then
      color=$clean
    fi

    if [[ $git_status =~ 'Your branch is ahead' ]]; then
      color=$ahead
      branch="${branch}>"
    fi

    if [[ $git_status =~ 'Your branch is behind' ]]; then
      color=$behind
      branch="${branch}<"
    fi

    if [[ $git_status =~ 'Changes to be committed' ]]; then
      color=$staged
    fi

    if [[ $git_status =~ 'Changed but not updated' ||
          $git_status =~ 'Changes not staged'      ||
          $git_status =~ 'Unmerged paths' ]]; then
      color=$dirty
    fi

    if [[ $git_status =~ 'Your branch'.+diverged ]]; then
      color=$diverged
      branch="${branch}!"
    fi

    # Print the colored branch name + indicators
    printf "\[\033[%sm\]%s" "$color" "$branch"

    # Reset the color
    printf "\[\033[0m\]"
  fi

  return 0
}

_show_last_exit_status() {
  # Display the exit status of the last run command
  exit_status=$?
  if [[ "$exit_status" -ne 0 ]]; then
    echo "Exit $exit_status"
  fi
}

# set the command that runs after every command to create the prompt
export PROMPT_COMMAND="_show_last_exit_status; _build_prompt; history -a"

# ex: tabstop=2 shiftwidth=2 softtabstop=2 expandtab filetype=sh
